//
//  GraphFunc.c
//  GraphFunc
//
//  Created by Daria Shumova on 27.11.2017.
//  Copyright © 2017 Daria Shumova. All rights reserved.
//

#include "GraphFunc.h"


//------------------ 1. Написать функции, которые считывают матрицу смежности из файла и выводят ее на экран.
void printMatrix (Graph_int graph)
{
    for (int i = 0; i < graph.vertex; i++)
    {
        for (int j = 0; j < graph.vertex; j++)
            printf("%3i ", *(graph.ptr + i * graph.vertex + j));
        
        printf("\n");
    }
}


void scanMatrix (Graph_int *graph, char path[])
{
    FILE *file;
    
    if (!(file = fopen(path, "r+")))
        printf("Cannot open file.\n");
    else
    {
        fscanf(file, "%i", &graph->vertex);
        
        printf("number of vertex = %i\n", graph->vertex);
        
        graph->ptr = (int *) malloc(graph->vertex * graph->vertex * sizeof(int));
        
        for (int i = 0; i < graph->vertex; i++)
            for (int j = 0; j < graph->vertex; j++)
                fscanf(file, "%i", (graph->ptr + i * graph->vertex + j));
        fclose(file);
    }
}

//----------------------2. Написать  рекурсивную  функцию  обхода графа в глубину.
void dfs (Graph_int *graph, int vertex, Array_int *visited)
{
    if (vertex < graph->vertex)
    {
        visited->ptr[vertex] = 1;
        
        for (int j = 0; j < graph->vertex; j++)
            if (*(graph->ptr + vertex * graph->vertex + j) && !visited->ptr[j])
                dfs(graph, j, visited);
    }
}

//----------------------3. Написать  функцию  обхода г рафа  в  ширину.
void bfs (Graph_int *graph, Array_int *visited)
{
    Queue someQueue;
    int vertex = 0;
    
    someQueue.head = NULL;
    someQueue.size = 0;
    someQueue.maxSize = 100;
    
    visited->ptr[vertex] = 1;
    enqueue(&someQueue, vertex);
    
    while (someQueue.size)
    {
        int temp = dequeue(&someQueue);
        
        for (int j = 0; j < graph->vertex; j++)
            if (*(graph->ptr + temp * graph->vertex + j) && !visited->ptr[j])
            {
                visited->ptr[j] = 1;
                enqueue(&someQueue, j);
            }
    }
}


void printList (List *someList)
{
    while (someList)
    {
        printf("%c ", someList->value);
        someList = someList->next;
    }
}


void enqueue (Queue *queue, int newValue)
{
    if (queue->size >= queue->maxSize)
        printf("Queue is full");
    else
    {
        List *temp;
        temp = (List *)malloc(sizeof(List));
        if (!temp)
            printf("malloc() returned NULL");
        else
        {
            if (queue->size == 0)
            {
                temp->value = newValue;
                temp->next = NULL;
                queue->head = temp;
                queue->tail = queue->head;
                queue->size++;
            }
            else
            {
                temp->value = newValue;
                temp->next = NULL;
                queue->tail->next = temp;
                queue->tail = queue->tail->next;
                queue->size++;
            }
        }
    }
}


int dequeue (Queue *queue)
{
    int result = 0;
    if (!queue->size)
        printf("Queue is empty");
    else
    {
        List *temp;
        temp = queue->head;
        result = queue->head->value;
        queue->head = queue->head->next;
        free(temp);
        queue->size--;
    }
    return result;
}

