//
//  GraphFunc.h
//  GraphFunc
//
//  Created by Daria Shumova on 27.11.2017.
//  Copyright © 2017 Daria Shumova. All rights reserved.
//

#ifndef GraphFunc_h
#define GraphFunc_h

#include <stdio.h>
#include <mm_malloc.h>

typedef struct{
    int *ptr;
    u_int vertex;
} Graph_int;

typedef struct{
    int *ptr;
    u_int size;
} Array_int;

typedef struct List
{
    int value;
    struct List *next;
} List;

typedef struct {
    List *head;
    List *tail;
    u_int size;
    u_int maxSize;
} Queue;


void printList (List *someList);
void enqueue (Queue *queue, int newValue);
int dequeue (Queue *queue);

//----------- 1. Написать функции, которые считывают матрицу смежности из файла и выводят ее на экран.
void printMatrix (Graph_int graph);
void scanMatrix (Graph_int *graph, char path[]);
//------------2. Написать  рекурсивную  функцию  обхода графа в глубину.
void dfs (Graph_int *graph, int vertex, Array_int *visited);
//------------3. Написать  функцию  обхода г рафа  в  ширину.
void bfs (Graph_int *graph, Array_int *visited);

#endif /* GraphFunc_h */
