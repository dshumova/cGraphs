//
//  main.c
//  HW-7
//
//  Created by Daria Shumova on 24.11.2017.
//  Copyright © 2017 Daria Shumova. All rights reserved.
//

#include <stdio.h>
#include "GraphFunc/GraphFunc.h"


int main(int argc, const char * argv[]) {
    
    Graph_int graph;
    Array_int visited;
    char path[] = "/Users/dariashumova/Documents/adjacencyMatrix.txt";
    
    // ------- 1. -----------------
    scanMatrix(&graph, path);
    printMatrix(graph);
    
    // -------- 2. -------------------
    visited.size = graph.vertex;
    visited.ptr = (int *) malloc(visited.size * sizeof(int));
    for (int i = 0; i < visited.size; i++)
        visited.ptr[i] = 0;
    
    printf("\nresult dfs: ");
    dfs(&graph, 0, &visited);
    for (int i = 0; i < visited.size; i++)
        printf("%i ", visited.ptr[i]);
    printf("\n");
    
    // ---------- 3. ----------------------
    for (int i = 0; i < visited.size; i++)
        visited.ptr[i] = 0;
    bfs(&graph, &visited);
    printf("\nresult bfs: ");
    for (int i = 0; i < visited.size; i++)
        printf("%i ", visited.ptr[i]);
    printf("\n");
    
    return 0;
}
